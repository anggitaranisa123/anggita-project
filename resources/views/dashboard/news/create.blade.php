@extends('layouts.app2')
@section('title', 'Tambah Data News')

@section('title-header', 'Tambah Data News')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route('news.index') }}">Data News</a></li>
    <li class="breadcrumb-item active">Tambah Data News</li>
@endsection

@section('c_css')
<script src="https://cdn.ckeditor.com/4.19.0/full-all/ckeditor.js"></script>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card shadow">
                <div class="card-header bg-transparent border-0 text-dark">
                    <h5 class="mb-0">Formulir Tambah Data News</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('news.store') }}" method="POST" role="form" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group mb-3">
                            <label for="title">Title</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title"
                                placeholder="Title News" value="{{ old('title') }}" name="title">

                            @error('title')
                                <div class="d-block invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="content">Content</label>
                            <textarea  class="form-control @error('content') is-invalid @enderror" id="ckeditor-backend"
                            placeholder="Content News" name="content" cols="30" rows="10">{!! old('content') !!}</textarea>
                            @error('content')
                                <div class="d-block invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="image">Image Thumbnail</label>
                            <input type="file" class="form-control dropify-media @error('image') is-invalid @enderror"
                                id="image" placeholder="Image Thumbnail News"
                                name="image">

                            @error('image')
                                <div class="d-block invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <button type="submit" class="btn btn-sm btn-primary">Tambah</button>
                                <a href="{{route('news.index')}}" class="btn btn-sm btn-secondary">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        CKEDITOR.replace('ckeditor-backend')
        $('.dropify-media').dropify({
            messages: {
                default: 'Drag and drop a file here or click',
                replace: 'Drag and drop or click to replace',
                remove: 'Remove',
                error: 'Ooops, something wrong happended.'
            },
        });
    </script>
@endsection