@extends('layouts.app2')
@section('title', 'Dashboard')
@section('title-header', 'Dashboard')
@php
    $auth = Auth::user();
@endphp

@section('breadcrumb')
<li class="breadcrumb-item active"><a
href="{{ route('home') }}">Dashboard</a></li>
@endsection
