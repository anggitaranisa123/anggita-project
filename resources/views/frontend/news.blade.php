<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>News</title>
    <link rel="stylesheet" href="{{ asset('/frontend') }}/css/style1.css">
    <link rel="stylesheet" href="{{ asset('/frontend') }}/css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
</head>

<body>
    <x-navbar />
    <header class="newss">
        <div class="d-flex justify-content-center align-items-center h-100 text-light fw-bold">
            <h1 class="news">NEWS</h1>
        </div>
    </header>

    <section id="news">
        <div class="container mt-5">
            <div class="p-3">
                <div class="row">
                    <div class="col-sm-12 col-md-8">
                        @foreach ($news as $item)
                            @php
                                $images = asset('/uploads/images/' . $item->image);
                                if (is_null($item->image)) {
                                    $images = asset('/uploads/images/default.png');
                                }
                            @endphp
                            <div class="card mb-5 shadow" style="max-width: 700px;">
                                <div class="row g-0">
                                    <div class="col-md-4">
                                        <img src="{{ $images }}" class="img-fluid img-news w-100 h-100"
                                            alt="{{ $item->title }}">
                                    </div>
                                    <div class="col-md-8 ">
                                        <div class="card-body p-4">
                                            <h5 class="card-subtitle mb-2">
                                                {{ date('Y.m.d', strtotime($item->created_at)) }}
                                            </h5>
                                            <a class="card-title text-uppercase h5 text-decoration-none text-dark"
                                                href="{{ route('frontend.news-detail', ['newsId' => $item->id]) }}">{{ $item->title }}</a>
                                            <p class="card-text">
                                                {!! $item->content
                                                    ? str()->limit($item->content, 150)
                                                    : 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam odio similique doloribus ut neque voluptatem sint nisi, cumque ab quas quo beatae adipisci quidem, nobis distinctio voluptatibus minus commodi labore, sequi laudantium eveniet magnam blanditiis eos? Incidunt, cum distinctio temporibus est repellat eos voluptatum dolorum, nobis assumenda quidem placeat ' !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {!! $news->links() !!}
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <h2 class="text-danger fw-bold text-decoration-underline mb-5">YOU MIGHT ALSO LIKE</h2>
                        @foreach ($relatedNews as $item)
                            @php
                                $images = asset('/uploads/images/' . $item->image);
                                if (is_null($item->image)) {
                                    $images = asset('/uploads/images/default.png');
                                }
                            @endphp
                            <div class="card mb-5 shadow" style="max-width: 700px;">
                                <img src="{{ $images }}" class="card-img-top" alt="{{ $item->title }}">
                                <div class="card-body p-4">
                                    <h5 class="card-subtitle mb-2">
                                        {{ date('Y.m.d', strtotime($item->created_at)) }}
                                    </h5>
                                    <a class="card-title mb-2 h5 text-decoration-none text-dark"
                                        href="{{ route('frontend.news-detail', ['newsId' => $item->id]) }}">
                                        {{ $item->title }}
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="Global" class="text-center mt-5 mb-5">
        <div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active" data-bs-interval="1500">
                    <img src="{{ asset('/frontend') }}/img/Global.png" class=" img-fluid" alt="...">
                </div>
                <div class="carousel-item" data-bs-interval="1500">
                    <img src="{{ asset('/frontend') }}/img/global2.png" class=" img-fluid" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('/frontend') }}/img/global3.png" class=" img-fluid" alt="...">
                </div>
            </div>
        </div>
    </section>

    <x-footer />

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js"
        integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous">
    </script>
</body>

</html>
