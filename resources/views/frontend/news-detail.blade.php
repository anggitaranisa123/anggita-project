<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>News Detail - {{ $news->title }}</title>
    <link rel="stylesheet" href="{{ asset('/frontend') }}/css/style1.css">
    <link rel="stylesheet" href="{{ asset('/frontend') }}/css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
</head>

<body>
    <x-navbar />
    <header class="detail-news">
        <img src="{{ asset('/uploads/images/' . $news->image) }}" class="detail-image img-fluid"
            alt="{{ $news->title }}">
    </header>

    <section id="detail-news">
        <div class="container mt-5">
            <div class="p-3">
                <div class="row">
                    <div class="news1 col-sm-12 col-md-8">
                        <h1 class="fw-bold mb-3 text-upppercase">{{ $news->title }}
                        </h1>
                        <h4 class="bls p-2 d-flex">{{ date('Y.m.d', strtotime($news->created_at)) }} by <div
                                class="card-subtitle ms-2">iwufteam</div>
                        </h4>
                        <p>
                            {!! $news->content !!}
                        </p>
                        <h2>
                            Share:
                            <i class="ms-2 bi bi-twitter text-primary"></i>
                            <i class="ms-2 bi bi-whatsapp text-success"></i>
                            <i class="ms-2 bi bi-facebook text-primary"></i>
                            <i class="ms-2 bi bi-line text-success"></i>
                            <i class="ms-2 bi bi-linkedin text-primary"></i>
                        </h2>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <h2 class="text-danger fw-bold text-decoration-underline mb-5">YOU MIGHT ALSO LIKE</h2>
                        @foreach ($relatedNews as $news)
                            @php
                                $images = asset('/uploads/images/' . $news->image);
                                if (is_null($news->image)) {
                                    $images = asset('/uploads/images/default.png');
                                }
                            @endphp
                            <div class="card mb-5 shadow" style="max-width: 700px;">
                                <img src="{{ $images }}" class="card-img-top" alt="{{ $news->title }}">
                                <div class="card-body p-4">
                                    <h5 class="card-subtitle mb-2">
                                        {{ date('Y.m.d', strtotime($news->created_at)) }}
                                    </h5>
                                    <a class="card-title text-upppercase text-dark text-decoration-none h5 text-dark
                                        href="{{ route('wushu.news-detail', $news->id) }}">
                                        {{ $news->title }}
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="Global" class="text-center mt-5 mb-5">
        <div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active" data-bs-interval="1500">
                    <img src="{{ asset('/frontend') }}/img/Global.png" class=" img-fluid" alt="...">
                </div>
                <div class="carousel-item" data-bs-interval="1500">
                    <img src="{{ asset('/frontend') }}/img/global2.png" class=" img-fluid" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('/frontend') }}/img/global3.png" class=" img-fluid" alt="...">
                </div>
            </div>
        </div>
    </section>

    <x-footer />

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js"
        integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous">
    </script>
</body>

</html>
