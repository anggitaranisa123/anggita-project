<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\News;
use Illuminate\Http\Request;

class RouteController extends Controller
{
    public function dashboard()
    {
        return view('dashboard.index');
    }

    public function frontendHome()
    {
        return view('frontend.home');
    }

    public function frontendNews()
    {
        $news = News::latest()->paginate(4);
        $relatedNews = News::latest()->paginate(3);
        return view('frontend.news', compact('news', 'relatedNews'));
    }

    public function frontendNewsDetail($newsId)
    {
        $news = News::findOrFail($newsId);
        $relatedNews = News::latest()->paginate(3);
        return view('frontend.news-detail', compact('news', 'relatedNews'));
    }

    public function frontendGaleries()
    {
        $galeries = Image::whereNull('parent_id')->latest()->paginate(6);
        return view('frontend.galeries', compact('galeries'));
    }

    public function frontendGaleriesDetail($galeryId)
    {
        $detailGalery = Image::findOrFail($galeryId);
        $galeries = Image::whereNotNull('parent_id')->whereParentId($galeryId)->paginate(12);
        return view('frontend.galeries-detail', compact('galeries', 'detailGalery'));   
    }

    public function frontendAbout($who)
    {
        $view = 'about-';
        if($who == str()->lower('8thWJWC')){
            $view .= str()->lower('8thWJWC');
        }elseif($who == str()->lower('IWUF')){
            $view .= str()->lower('IWUF');
        }else{
            $view .= 'tangerang';
        }

        return view('frontend.' . $view);
    }

    public function frontendCompetition($what)
    {
        $view = 'competition-';
        if($what == 'numbers'){
            $view .= 'numbers';
        }elseif($what == 'schedules'){
            $view .= 'schedules';
        }else{
            $view .= 'medals';
        }

        return view('frontend.' . $view);
    }
}
