<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RouteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# ------ Unauthenticated routes ------ #
Route::get('/', [RouteController::class, 'frontendHome'])->name('wushu.home');
Route::name('frontend.')->group(function(){
    Route::get('/news', [RouteController::class, 'frontendNews'])->name('news');
    Route::get('/news/detail/{newsId}', [RouteController::class, 'frontendNewsDetail'])->name('news-detail');
    Route::get('/galeries', [RouteController::class, 'frontendGaleries'])->name('galeries');
    Route::get('/galeries/detail/{galeryId}', [RouteController::class, 'frontendGaleriesDetail'])->name('galeries-detail');
    Route::get('/about/{who}', [RouteController::class, 'frontendAbout'])->name('about');
    Route::get('/competition/{what}', [RouteController::class, 'frontendCompetition'])->name('competition');
});
require __DIR__.'/auth.php';


# ------ Authenticated routes ------ #
Route::middleware('auth')->prefix('dashboard')->group(function() {
    Route::get('/', [RouteController::class, 'dashboard'])->name('home'); # dashboard

    Route::prefix('profile')->group(function(){
        Route::get('/', [ProfileController::class, 'myProfile'])->name('profile');
        Route::put('/change-ava', [ProfileController::class, 'changeFotoProfile'])->name('change-ava');
        Route::put('/change-profile', [ProfileController::class, 'changeProfile'])->name('change-profile');
    }); # profile group

    # ------ DataTables routes ------ #
    Route::prefix('data')->name('datatable.')->group(function(){
        Route::get('/users', [DataTableController::class, 'getUsers'])->name('users');
    });

    Route::resource('users', UserController::class);
    Route::resource('news', NewsController::class);
    Route::resource('galeries', ImageController::class);
});
